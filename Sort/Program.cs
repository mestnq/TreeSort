﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace Sort
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] data =
            {
                571, 50, 712, 134, -68, 235, 4, 820, 304, 764, 780, 929, 946, 448, 395, 609, 513, 635, 944, 348, 749,
                397, 283, 311, 692, 2, 704, 715, -841, 923, 258, 265, 2, 714, 376, 193, 240, 441, 657, 585, 197, 563,
                303, 963, 199, 114, 478, 848, 761, -678, 863, 255, 401, 471, 656, 966, 547, 596, 331, 198, 634, 188, 154,
                166, 618, 552, 2, 721, 196, 205, 992, 240, 61, 645, 444, 521, 551, 873, 481, 365, 647, 52, 352, 66, 117,
                867, 95, 256, 530, 977, 70, 94, 740, 56, 882, 701, 879, 46, 543, 787
            };
            Sorting.TreeSort(data);
        }
    }

    public class Sorting
    {
        public static void TreeSort<T>(T[] data)
        {
            Tree<T> tree = new Tree<T>();
            tree.AddNode(data[0]);
            Node<T> startNode = tree.ListNode[0]; //начальная нода
            for (int i = 1; i < data.Length; i++)
            {
                T currentItem = data[i]; //элемент массива который мы взяли
                CompareNode(currentItem, tree.ListNode, startNode);
            }

            List<Node<T>> LastPrintNode = new();
            PrintTree(startNode, tree, LastPrintNode);
        }

        private static void CompareNode<T>(T currentItem, List<Node<T>> listNode, Node<T> node)
        {
            if (IsItLess(node.Value, currentItem)) // node.Value < currentItem
            {
                if (node.RightSon == null)
                {
                    Node<T> newNode = new Node<T> {Value = currentItem, Parent = node};
                    listNode.Add(newNode);
                    node.AddRightSon(newNode);
                }
                else
                    CompareNode(currentItem, listNode, node.RightSon);
            }
            else
            {
                if (node.LeftSon == null)
                {
                    Node<T> newNode = new Node<T> {Value = currentItem, Parent = node};
                    listNode.Add(newNode);
                    node.AddLeftSon(newNode);
                }
                else
                    CompareNode(currentItem, listNode, node.LeftSon);
            }
        }

        private static void PrintTree<T>(Node<T> node, Tree<T> tree, List<Node<T>> LastPrintNode)
        {
            while (tree.ListNode.Count > 0)
            {
                if (node.LeftSon != null)
                    PrintTree(node.LeftSon, tree, LastPrintNode);
                else if (node.LeftSon == null)
                {
                    if (node.RightSon == null)
                    {
                        if (LastPrintNode.Count == 0 || !LastPrintNode.Contains(node))
                            Console.WriteLine(node.Value);
                        else if (LastPrintNode.Contains(node))
                            LastPrintNode.Remove(node);
                        tree.DeleteNode(node);
                        PrintTree(node.Parent, tree, LastPrintNode);
                    }
                    else if (node.RightSon != null)
                    {
                        Console.WriteLine(node.Value);
                        LastPrintNode.Add(node);
                        PrintTree(node.RightSon, tree, LastPrintNode);
                    }
                }
            }
        }

        private static bool IsItLess<T>(T a, T b) // а меньше б 
        {
            if (a is char)
            {
                return Convert.ToChar(a) < Convert.ToChar(b);
            }

            if (a is int)
            {
                return Convert.ToInt32(a) < Convert.ToInt32(b);
            }

            if (a is double)
            {
                return Convert.ToDouble(a) < Convert.ToDouble(b);
            }

            throw new Exception("I can sort only this type: char, int, double");
        }
    }

    public class Node<T>
    {
        public Node<T> Parent { get; set; }
        public Node<T> LeftSon { get; set; }
        public Node<T> RightSon { get; set; }
        public T Value { get; set; }

        public void AddLeftSon(Node<T> leftSon)
        {
            LeftSon = leftSon;
        }

        public void AddRightSon(Node<T> rightSon)
        {
            RightSon = rightSon;
        }
    }

    public class Tree<T>
    {
        public List<Node<T>> ListNode { get; }

        public Tree()
        {
            ListNode = new List<Node<T>>();
        }

        public void AddNode(T value)
        {
            ListNode.Add(new Node<T> {Value = value});
        }

        public void DeleteNode(Node<T> node)
        {
            if (node.Parent != null)
            {
                Node<T> parentNode = node.Parent;
                if (parentNode.LeftSon != null && node.Value.Equals(parentNode.LeftSon.Value))
                {
                    parentNode.LeftSon = null;
                }

                if (parentNode.RightSon != null && node.Value.Equals(parentNode.RightSon.Value))
                {
                    parentNode.RightSon = null;
                }
            }

            ListNode.Remove(node);
        }
    }
}